
package com.example.demo.object;

import lombok.Getter;
import lombok.Setter;



@Getter
@Setter
public class User {
    private String name;
    private Integer age;


    public User(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    public User(User user) {
        this.name= user.getName();
        this.age= user.getAge();
    }

    public User() {

    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}


