package com.example.demo.repository;

import com.example.demo.model.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface UserRepository extends JpaRepository<UserModel, Long> {
    @Query(value= "SELECT * FROM user_model WHERE age = ?1", nativeQuery = true)
    List<UserModel> search(String name);

    @Query(value = "SELECT * FROM user_model WHERE age = ?1", nativeQuery = true)
    List<UserModel> agesearch(Integer age);

    //Optional<UserModel> findUserByName(String name);
}


