

package com.example.demo.controller;

import com.example.demo.object.User;
import com.example.demo.service.FileService;
import com.example.demo.service.StatService;
import com.example.demo.service.StatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;


@RestController
@RequestMapping(path = "/artik")
public class ArtikController {

    private final Logger logger = LoggerFactory.getLogger(ArtikController.class);

    @Autowired
    FileService fileService;

    @Autowired
    StatService statService;

    // J'ai mis en dur pour éviter de réécrire dans swagger
    private static final String path = "src/main/resources/liste_noms_age.json";

    @GetMapping("/getSquare")
    public float getSquare(@RequestParam(value = "number", defaultValue = "0") float a) {
        return a * a;
    }

    @GetMapping("/getAllUsers")
    public User[] parseJson() {
        try {
            return fileService.parser(fileService.readFile(path));
        } catch (Exception e) {
            logger.error("error occurred while parsing", e);
            return null;
        }
    }

    @GetMapping("/getUserById")
    public User getUser(@RequestParam(value = "id") Integer id) {
        try {
            User[] users = fileService.parser(fileService.readFile(path));
            return users[id];
        } catch (Exception e) {
            logger.error("error occurred while parsing", e);
            return null;
        }

    }

    @GetMapping("/getStats")
    public String getStats() {
        try {
            User[] users = fileService.parser(fileService.readFile(path));
            StatUtils statUtils = new StatUtils(users);
            statUtils.compute();
            return statUtils.toString();
        } catch (Exception e) {
            logger.error("error occurred while computing", e);
            return null;

        }
    }

    //TODO Comprendre la différence entre le classe StatUtils et StatService
    @GetMapping("/getStatsService")
    public String getStatsService() {
        try {
            User[] users = fileService.parser(fileService.readFile(path));
            return statService.getAllStatsToString(users);
        } catch (Exception e) {
            logger.error("error occurred while computing", e);
            return null;

        }
    }


    //TODO Les méthodes suivantes sont à compléter
    @GetMapping("/getYoungestUser")
    public User getYoungestUser() {
        try {
            User[] users = fileService.parser(fileService.readFile(path));
            return statService.getYoungest(users);
        } catch (Exception e) {
            logger.error("error occurred while computing", e);
            return null;

        }
    }

    @GetMapping("/getOldestUser")
    public User getOldestUser() {
        try {
            User[] users = fileService.parser(fileService.readFile(path));
            return statService.getOldest(users);
        } catch (Exception e) {
            logger.error("error occurred while computing", e);
            return null;

        }

    }

    @GetMapping("/getAverageAge")
    public float getAverageAge() {
        try {
            User[] users = fileService.parser(fileService.readFile(path));
            return statService.getAverageAge(users);
        } catch (Exception e) {
            logger.error("error occurred while computing", e);
            return 0f;

        }
    }

    @GetMapping("/getLargestAgeGap")
    public float getLargestAgeGap() {
        try {
            User[] users = fileService.parser(fileService.readFile(path));
            return statService.getLargestAgeGap(users);
        } catch (Exception e) {
            logger.error("error occurred while computing", e);
            return 0f;

        }
    }
}







