package com.example.demo.controller;

import com.example.demo.model.UserModel;
import com.example.demo.object.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path="/user")

public class UserController {
    private final Logger logger = LoggerFactory.getLogger(ArtikController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/getUsers")
    public ResponseEntity<List<UserModel>> getUsers(){
        return ResponseEntity.ok(userService.getUsers());
    }

    @GetMapping("/getById/{id}")
    public ResponseEntity<UserModel> getById(@PathVariable Long id){
        return  ResponseEntity.ok(userService.getById(id));
    }
    @PostMapping("/addUser")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user){
        return  ResponseEntity.ok(userService.addUser(user));
    }
    // @GetMapping("/countUsers")
    // public ResponseEntity<Long> countUsers(){
    //     return ResponseEntity.ok(userService.count());
    // }

    @GetMapping("/getUserByName")
    public ResponseEntity<UserModel> findUserByName(@RequestParam(value = "nom") String name){
        if (userService.findUserByName(name)!= null)
            return ResponseEntity.ok(userService.findUserByName(name));
        else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    @GetMapping("/getUserByAge")
    public ResponseEntity<UserModel> findUserByAge(@RequestParam(value = "age") Integer age){
        if (userService.findUserByAge(age)!= null)
            return ResponseEntity.ok(userService.findUserByAge(age));
        else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @Autowired
    private KafkaTemplate<String, User>
            kafkaTemplate;

    private static final String TOPIC
            = "UserApp";

    @GetMapping("/publish/"
            + "{name}/{age}")

    public String post(
            @PathVariable("name") final
            String name,
            @PathVariable("age") final
            Integer age)
    {

        kafkaTemplate.send(
                TOPIC,
                new User(name,age));


        return "Published successfully";
    }

}
