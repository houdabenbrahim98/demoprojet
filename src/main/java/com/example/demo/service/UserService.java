package com.example.demo.service;

import com.example.demo.controller.ArtikController;
import com.example.demo.model.UserModel;
import com.example.demo.repository.UserRepository;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Getter
@Setter
@Service

public class UserService {

    private final Logger logger = LoggerFactory.getLogger(UserService.class);
    @Autowired
    private UserRepository userRepository;

    public List<UserModel> getUsers() {
        return userRepository.findAll();
    }

    public UserModel getById(Long id) {
        Optional<UserModel> userModelOptional = userRepository.findById(id);
        //logger.info("userRepository.findById(id) = {}", userRepository.findById(id));
        return userModelOptional.orElse(null);
    }

    public UserModel addUser(UserModel user) {
        return userRepository.save(user);
    }

    public UserModel findUserByName(String name) {
        List<UserModel> userOpt = userRepository.search(name);
        if (userOpt !=null && !userOpt.isEmpty())
            return userOpt.get(0);
        else return null;



        //public long count() {
        //   return userRepository.count();
        //}
    }
    public UserModel findUserByAge(Integer age) {
        List<UserModel> userOpt1 = userRepository.agesearch(age);
        if (userOpt1 !=null && !userOpt1.isEmpty())
            return userOpt1.get(0);
        else return null;
    }
}
