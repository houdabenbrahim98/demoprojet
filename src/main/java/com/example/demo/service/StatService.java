package com.example.demo.service;

import com.example.demo.object.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.IntStream;

@Service
public class StatService {

    private Logger logger = LoggerFactory.getLogger(StatService.class);
    
    public User getYoungest(User[] users){
        User youngestUser = new User();
        try {
            youngestUser = new User(users[0]);
            for (User user : users) {
                if (youngestUser.getAge() > user.getAge()) {
                    youngestUser = new User(user);
                }
            }
        } catch (Exception e){
            logger.error("Error in getYoungest", e);
        }
        return youngestUser;
    }

    public User getYoungestStream(User[] users){
        return Arrays.stream(users)
                .min(Comparator.comparing(User::getAge))
                .get();
    }

    public User getOldest(User[] users){
        User oldestUser = new User();
        try {
            oldestUser = new User(users[0]);
            for (User user : users) {
                if (oldestUser.getAge() < user.getAge()) {
                    oldestUser = new User(user);
                }
            }
        } catch (Exception e){
            logger.error("Error in getOldest", e);
        }
        return oldestUser;
    }

    public User getOldestStream(User[] users){
        return Arrays.stream(users)
                .max(Comparator.comparing(User::getAge))
                .get();
    }

    public float getAverageAge(User[] users){
        int averageAge = 0;
        try {
            for (User User: users){
                averageAge += User.getAge();
            }
        } catch (Exception e){
            logger.error("Error in getAverageAge", e);
        }
        return (float) averageAge/users.length;
    }

    public float getAverageAgeStream(User[] users){
        return (float) Arrays.stream(users)
                .mapToDouble(User::getAge)
                .average()
                .getAsDouble();
    }

    public int getLargestAgeGap(User[] users){
        int gap = 0;
        try {
            gap = users[1].getAge() - users[0].getAge();
            for (int i = 1; i < users.length-1; i++){
                if (gap < users[i+1].getAge() - users[i].getAge()){
                    gap = users[i+1].getAge() - users[i].getAge();
                }
            }
        } catch (Exception e){
            logger.error("Error in getLargestAgeGap", e);
        }
        return gap;
    }

    public int getLargestAgeGapStream(User[] users){
        return IntStream
                .range(0, users.length -1)
                .map(i -> users[i+1].getAge()-users[i].getAge())
                .max()
                .getAsInt();
    }

    public String getAllStatsToString(User[] users){
        return "YoungestUser: " + getYoungest(users) + "\n"+
                "YoungestUserStream: " + getYoungestStream(users) + "\n"+
                "\n"+
                "OldestUser: " + getOldest(users)+ "\n"+
                "OldestUserStream: " + getOldestStream(users)+ "\n"+
                "\n"+
                "AverageAge: " + getAverageAge(users)+"\n"+
                "AverageAgeStream: " + getAverageAgeStream(users)+"\n"+
                "\n"+
                "LargestAgeGap: " + getLargestAgeGap(users)+"\n"+
                "LargestAgeGapStream: " + getLargestAgeGapStream(users);
    }
}

