package com.example.demo.service;

import com.example.demo.object.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.IntStream;

public class StatUtils {

    private User[] users;
    private User youngest;
    private User youngestStream;
    private User oldest;
    private User oldestStream;
    private float averageAge;
    private float averageAgeStream;
    private int largestAgeGap;
    private int largestAgeGapStream;

    private Logger logger = LoggerFactory.getLogger(StatUtils.class);

    public StatUtils(User[] users) {
        this.users = users;
    }

    public User computeYoungest(){
        User youngestUser = new User();
        try {
            youngestUser = new User(this.users[0]);
            for (User user : this.users) {
                if (youngestUser.getAge() > user.getAge()) {
                    youngestUser = new User(user);
                }
            }
        } catch (Exception e){
            logger.error("Error in getYoungest", e);
        }
        return youngestUser;
    }

    public User computeYoungestStream(){
        return Arrays.stream(this.users)
                .min(Comparator.comparing(User::getAge))
                .get();
    }

    public User computeOldest(){
        User oldestUser = new User();
        try {
            oldestUser = new User(this.users[0]);
            for (User user : this.users) {
                if (oldestUser.getAge() < user.getAge()) {
                    oldestUser = new User(user);
                }
            }
        } catch (Exception e){
            logger.error("Error in getOldest", e);
        }
        return oldestUser;
    }

    public User computeOldestStream(){
        return Arrays.stream(this.users)
                .max(Comparator.comparing(User::getAge))
                .get();
    }

    public float computeAverageAge(){
        int averageAge = 0;
        try {
            for (User User: this.users){
                averageAge += User.getAge();
            }
        } catch (Exception e){
            logger.error("Error in getAverageAge", e);
        }
        return (float) averageAge/this.users.length;
    }

    public float computeAverageAgeStream(){
        return (float) Arrays.stream(this.users)
                .mapToDouble(User::getAge)
                .average()
                .getAsDouble();
    }

    public int computeLargestAgeGap(){
        int gap = 0;
        try {
            gap = this.users[1].getAge() - this.users[0].getAge();
            for (int i = 1; i < users.length-1; i++){
                if (gap < this.users[i+1].getAge() - this.users[i].getAge()){
                    gap = this.users[i+1].getAge() - this.users[i].getAge();
                }
            }
        } catch (Exception e){
            logger.error("Error in getLargestAgeGap", e);
        }
        return gap;
    }

    public int computeLargestAgeGapStream(){
        return IntStream
                .range(0, this.users.length -1)
                .map(i -> this.users[i+1].getAge()-this.users[i].getAge())
                .max()
                .getAsInt();
    }

    public void compute(){
        this.setYoungest(this.computeYoungest());
        this.setYoungestStream(this.computeYoungestStream());
        this.setOldest(this.computeOldest());
        this.setOldestStream(this.computeOldestStream());
        this.setAverageAge(this.computeAverageAge());
        this.setAverageAgeStream(this.computeAverageAgeStream());
        this.setLargestAgeGap(this.computeLargestAgeGap());
        this.setLargestAgeGapStream(this.computeLargestAgeGapStream());
    }

    /***
     * getter and setters
     */

    public User[] getUsers() {
        return users;
    }
    public void setUsers(User[] users) {
        this.users = users;
    }

    public User getYoungest() {
        return youngest;
    }
    public void setYoungest(User youngest) {
        this.youngest = youngest;
    }

    public User getYoungestStream() {
        return youngestStream;
    }
    public void setYoungestStream(User youngestStream) {
        this.youngestStream = youngestStream;
    }

    public User getOldest() {
        return oldest;
    }
    public void setOldest(User oldest) {
        this.oldest = oldest;
    }

    public User getOldestStream() {
        return oldestStream;
    }
    public void setOldestStream(User oldestStream) {
        this.oldestStream = oldestStream;
    }

    public float getAverageAge() {
        return averageAge;
    }
    public void setAverageAge(float averageAge) {
        this.averageAge = averageAge;
    }

    public float getAverageAgeStream() {
        return averageAgeStream;
    }
    public void setAverageAgeStream(float averageAgeStream) {
        this.averageAgeStream = averageAgeStream;
    }

    public int getLargestAgeGap() {
        return largestAgeGap;
    }
    public void setLargestAgeGap(int largestAgeGap) {
        this.largestAgeGap = largestAgeGap;
    }

    public int getLargestAgeGapStream() {
        return largestAgeGapStream;
    }
    public void setLargestAgeGapStream(int largestAgeGapStream) {
        this.largestAgeGapStream = largestAgeGapStream;
    }

    public String toString(){
        return "YoungestUser: " + this.getYoungest() + "\n"+
                "YoungestUserStream: " + this.getYoungestStream() + "\n"+
                "\n"+
                "OldestUser: " + this.getOldest()+ "\n"+
                "OldestUserStream: " + this.getOldestStream()+ "\n"+
                "\n"+
                "AverageAge: " + this.getAverageAge()+"\n"+
                "AverageAgeStream: " + this.getAverageAgeStream()+"\n"+
                "\n"+
                "LargestAgeGap: " + this.getLargestAgeGap()+"\n"+
                "LargestAgeGapStream: " + this.getLargestAgeGapStream();
    }
}

