package com.example.demo.service;

import com.example.demo.object.User;
import com.jayway.jsonpath.JsonPath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;


@Service
public class FileService {
    private Logger logger = LoggerFactory.getLogger(FileService.class);


    /**
     Read file with scanner
     Read Json and parse json
     */

    public String readFile(String path) {
        String content = "";
        try {
            content = new String(Files.readAllBytes(Paths.get(path)), StandardCharsets.UTF_8);
        } catch (FileNotFoundException e){
            e.printStackTrace();
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return content;
    }

    public User[] parser(String jsonString) {
        try {
            return JsonPath.parse(jsonString).read("$.users[*]", User[].class);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
